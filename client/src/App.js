import React from 'react';
import SignUp from './components/user/SignUp';
import { Switch, Route } from 'react-router-dom';
import LogIn from './components/user/LogIn';
import Home from './components/home/Home';
import MovieList from './components/movie/MovieList';
import EditMovie from './components/movie/EditMovie';
import AddMovie from './components/movie/AddMovie';


function App() {
  return (
    <Switch>
      <Route path='/movies/add'>
        <AddMovie />
      </Route>
      <Route path="/movies/:id/edit">
        <EditMovie />  
      </Route>      
      <Route path="/signup" >
        <SignUp />
      </Route>
      <Route path="/login" >
        <LogIn />
      </Route>
      <Route path="/movies" >
        <MovieList />
      </Route>
      <Route path='/' exact>
        <Home />
      </Route>
    </Switch>
  );
}

export default App;
