import React from 'react'

export default function AuthForm(props) {
    return (
    <div>
        <form onSubmit={props.submitHandler}>
            <label htmlFor="email">Email</label>
            <input type="text" name="email" />
            <label htmlFor="password">Password</label>
            <input type="password" name="password" />
            <button>Sign up</button>
        </form>
    </div>
    )
}
