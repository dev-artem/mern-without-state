import React, { Component } from 'react'
import AuthForm from './AuthForm';
import {Redirect } from 'react-router-dom';

export default class LogIn extends Component {
    constructor(props){
        super(props);
        this.state = {
            redirect: false
        }
        
        this.submitHandler = this.submitHandler.bind(this);
    }

    async submitHandler(e){
        e.preventDefault();
        const email = e.target.email.value;
        const password = e.target.password.value;
        const res = await fetch('/login', {
            method: 'POST',
            body: JSON.stringify({email, password}),
            headers: {'Content-Type': 'application/json'}
        });
        const data = await res.json();
        this.setState({
            redirect: true
        });
    }

    render() {
        if(this.state.redirect){
            return (
                <Redirect to='/' />
            );
        }
        return (
            <AuthForm submitHandler={this.submitHandler} />
        )
    }
}
