import React, { Component } from 'react'
import MovieItem from './MovieItem';
import { Link } from 'react-router-dom'

export default class MovieList extends Component {
    constructor(props){
        super(props);
        this.state = {
            list: []
        }
    }

    async componentDidMount(){
        const res = await fetch('/api/movies', {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        });

        const list = await res.json();
        this.setState({
            list
        });
    }

    render() {

        if(this.state.list.length === 0){
            return(
                <div>
                    <h2>No movies</h2>
                </div>
            );
        }

        const movies = this.state.list.map((movie) => {
            return <MovieItem key={movie._id} movie={movie} />
        })
        return (
            <div>
                <Link to='/movies/add'>Add movie</Link>
                {movies}
            </div>
        )
    }
}
