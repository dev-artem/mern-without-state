import React from 'react'
import {Link} from 'react-router-dom'


export default function MovieItem(props) {
    const {_id, title, desc, year, img} = props.movie;
    async function del(e){
        e.preventDefault();
        const res = await fetch(`/api/movies/${_id}`, {
            method: 'DELETE'
        })
    }

    return (
        <div className="movie_item">
            <img className='movie_img' src={img} />
            <h2>{title}<span>({year})</span></h2>
            <p>{desc}</p>
            <Link to={{
                pathname: `/movies/${_id}/edit`
            }}>Edit</Link>
            <button onClick={del}>Delete</button>
        </div>
    )
}
