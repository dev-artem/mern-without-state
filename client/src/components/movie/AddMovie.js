import React, {useState} from 'react'
import MovieForm from './MovieForm';
import { Redirect } from 'react-router-dom'



export default function AddMovie() {

    const [state, setState] = useState(false);

    async function create(e){
        e.preventDefault();
        const title = e.target.title.value;
        const year = e.target.year.value;
        const desc = e.target.desc.value;
        const img = e.target.img.value;
    
        await fetch('/api/movies', {
            method: 'POST',
            body: JSON.stringify({title,desc,year,img}),
            headers: {'Content-Type': 'application/json'}
        });
        setState(true);
    }

    if(state){
        return <Redirect to='/movies' />
    }
    return (
        <MovieForm handleSubmit={create} />
    )
}
