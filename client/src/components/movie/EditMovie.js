import React, {useState} from 'react'
import MovieForm from './MovieForm';
import {  useParams, Redirect } from "react-router-dom";


export default function EditMovie() {

    const [state, setState] = useState(false);

    let { id } = useParams();
    
    async function edit(e){
        e.preventDefault();
        const title = e.target.title.value;
        const year = e.target.year.value;
        const desc = e.target.desc.value;
        const img = e.target.img.value;
    
        await fetch(`/api/movies/${id}`, {
            method: 'PUT',
            body: JSON.stringify({title,desc,year,img}),
            headers: {'Content-Type': 'application/json'}
        });
        setState(true);
    }
    if(state){
        return <Redirect to='/movies' />
    }
    return (
        <MovieForm movie={id} handleSubmit={edit} />
    )
}


