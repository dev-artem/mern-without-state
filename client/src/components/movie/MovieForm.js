import React, { Component } from 'react'

export default class MovieForm extends Component {
    constructor(props){
        super(props);

        this.state = {
            title: '',
            desc: '',
            year: null,
            img: ''
        }
    }

    async componentDidMount(){
        if(this.props.movie){
            const res = await fetch(`/api/movies/${this.props.movie}`, {
                method: 'GET',
                headers: {'COntent-Type': 'application/json'}
            });
            const movie = await res.json();
            const {title, desc, year, img} = movie
            this.setState({title, desc, year, img});
        }
    }

    render() {
        return (
            <form onSubmit={this.props.handleSubmit}>
               <label htmlFor="title">Title:</label>
               <input name="title" type="text" defaultValue={this.state.title} />

               <label htmlFor="year">Year:</label>
               <input name="year" type="number" defaultValue={this.state.year} />

               <label htmlFor="desc">Description:</label>
               <textarea name="desc" defaultValue={this.state.desc} />

               <label htmlFor="img">Img:</label>
               <input name="img" type="text" defaultValue={this.state.img} />
               <button>Submit</button>
            </form>
        )
    }
}
