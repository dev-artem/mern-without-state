import React, { Component } from 'react'
import { Redirect } from 'react-router-dom';


export default class Navbar extends Component {
    constructor(props){
        super(props);
        this.state = {
            redirect: false
        }

        this.logout = this.logout.bind(this);
    }

    async logout(e){
        e.preventDefault();
        const res = await fetch('/logout', {
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        });
        this.setState({redirect: true});
    }



    render() {

        let auth = this.state.redirect 
        ? (<>
                <li><a href="/login" >Log in</a></li>
                <li><a href="/signup" >Sign up</a></li>
                <Redirect to='/' />
                </>) 
        : (
            <li><button onClick={this.logout} >Log out</button></li>
        );
        
        return (
            <div>
                <ul>
                    <li><a href='/'>Home</a></li>
                {auth}                
                </ul>
            </div>
        );
    }
}

