const jwt = require('jsonwebtoken');
const { userSchema } = require("../helpers/user-validation");
const User = require("../models/User");


const handleErrors = (err) => {
    let errors = { email: '', password: '', validation: ''}

    if(err.message === 'Invalid email'){
        errors.email === 'This email not registred yet';
    }

    if(err.message === 'Invalid password'){
        errors.password = 'This password is incorrect';
    }

    if(err.message === 'Email already exist'){
        errors.email = 'This email is already registred'
    }

    if(err.isJoi){
        errors.validation = err.details[0].message;
    }

    return errors;
}


const maxAge = 24 * 60 * 60;

const createToken = (id) => {
    return jwt.sign({id}, process.env.SECRET, {
        expiresIn: maxAge
    })
}



module.exports.signup = async (req, res) => {
    try{
        console.log(req.body);
        const validation = await userSchema.validateAsync(req.body);
        const checkEmail = await User.findOne({email: validation.email});
        if(checkEmail){
            throw Error('Email already exist');    
        }
        const user = await User.create(validation);
        const token = createToken(user._id);
        res.cookie('jwt', token, {httpOnly: true, maxAge: maxAge * 1000});
        res.status(201).json({user: user._id});
    
    } catch (err){
        const errors = handleErrors(err);
        res.status(400).json({errors});
    }
}


module.exports.login = async (req, res) => {
    try {
        const validation = await userSchema.validateAsync(req.body);
        const user = await User.login(validation.email, validation.password);
        const token = createToken(user._id);
        res.cookie('jwt', token, {httpOnly: true, maxAge: maxAge * 1000});
        res.status(200).json({user: user._id});
    } catch (err) {
        const errors = handleErrors(err);
        res.status(400).json({errors});
    }
}

module.exports.logout = (req, res) => {
    res.cookie('jwt', '', {maxAge: 1});
    res.redirect('/');
}