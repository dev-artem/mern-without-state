const Movie = require('../models/Movie');


module.exports.get_movies = async (req, res) => {
    try{
        const list = await Movie.find({});
        res.status(200).json(list);
    } catch (err){
        res.status(400).send();
    }
}

module.exports.get_movie = async (req, res) => {
    try {
        const id = req.params.id;
        const movie = await Movie.findOne({_id: id});
        res.status(200).json(movie);
    } catch (err) {
        res.status(400).send();
    }
}

module.exports.add_movie = async (req, res) => {
    try {
        const {title, desc, year, img} = req.body;
        const movie = await Movie.create({title, desc, year, img})
        res.status(201).json({movie: movie._id});
    } catch (err) {
        res.status(400).send();
    }
}

module.exports.update_movie = async (req, res) => {
    try {
        const id = req.params.id;
        const movie = await Movie.findOne({_id: id});
        const {title, desc, year, img} = req.body;
        if(title){
            movie.title = title;
        }
        if(desc){
            movie.desc = desc;
        }
        if(year){
            movie.year = year;
        }
        if(img){
            movie.img = img;
        }
        await movie.save();
        res.status(200).json({movie: movie._id});
    } catch (err) {
        res.status(400).send();
    }
}

module.exports.delete_movie = async (req, res) => {
    try {
        const id = req.params.id;
        const del = await Movie.deleteOne({_id: id});
        res.status(200).json({del: id});
    } catch (err) {
        res.status(400).send();
    }
}