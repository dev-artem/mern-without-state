const { Router } = require('express');
const movieController = require('../controllers/movieController');

const movieRoutes = Router();

movieRoutes.get('/movies', movieController.get_movies);
movieRoutes.get('/movies/:id', movieController.get_movie);
movieRoutes.post('/movies', movieController.add_movie);
movieRoutes.put('/movies/:id', movieController.update_movie);
movieRoutes.delete('/movies/:id', movieController.delete_movie);

module.exports = movieRoutes;