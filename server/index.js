const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');

const authRouter = require('./routes/authRoutes');
const movieRouter = require('./routes/movieRoutes');


const app = express();


// middlewares
app.use(express.static(path.join(__dirname, 'public')))
app.use(morgan('tiny'));
app.use(express.json());
app.use(cookieParser());

// routes
app.use(authRouter);
app.use('/api', movieRouter)



const PORT = process.env.PORT || 3001;

mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
})
.then(() => {
    app.listen(PORT, () => {
        console.log(`Server is running on port: ${PORT}`)
    })
})
.catch((err) => {
    console.log(err);
});
