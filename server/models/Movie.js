const {model, Schema} = require('mongoose');

const movieSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    desc: {
        type: String,
        required: true
    },
    year: {
        type: Number,
        minlength: 4
    },
    img: {
        type: String,
        default: 'https://region4.uaw.org/sites/default/files/bio/10546i3dac5a5993c8bc8c_4.jpg'
    }
});

module.exports = model('Movie', movieSchema);